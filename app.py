from datetime import date
import telebot
from pathlib import Path
import os
import json
import requests
import functions

reports_path = "./reports/"
if not os.path.exists(reports_path):
    os.mkdir(reports_path)

with open('credentials.json', 'rb') as file:
    credentials = json.loads(file.read())
    API_KEY = credentials['API_KEY']
    admins = credentials['admin_IDs']
    nexus_login = credentials['nexus_login']
    nexus_password = credentials['nexus_password']
    agent_pipeline_token = credentials['agent_pipeline_token']
    argus_pipeline_token = credentials['argus_pipeline_token']
    tags = credentials['tags']
    chat_IDs = credentials['chat_IDs']

current_chat_ID = 'dsp'
bot = telebot.TeleBot(API_KEY)

command_list = ['help', 'nexus_get_reports', 'nexus_upload_reports', 'nexus_rotation', 'run_agent_pipeline',
                'run_argus_pipeline', 'set_timer_event', 'get_timer_event', 'clear_local_reports', 'get_local_reports',
                'get_local_report']
today = date.today().strftime("%d_%m_%Y")

bot.set_my_commands([
    telebot.types.BotCommand("/clear_local_reports", "очищает локальную директорию с отчетами"),
    telebot.types.BotCommand("/get_local_reports", "получает имена локальных отчетов"),
    telebot.types.BotCommand("/get_local_report", "получает локальный отчет по имени"),
    telebot.types.BotCommand("/get_timer_event", "запрашивает дату и время следующего события таймера"),
    telebot.types.BotCommand("/set_timer_event", "устанавливает день недели автоматического цикла сборки,"
                                                 " 0 - понедельник, числа > 6 - выключают автоматическую сборку"),
    telebot.types.BotCommand("/run_argus_pipeline", "запускает сборку аргуса"),
    telebot.types.BotCommand("/run_agent_pipeline", "запускает сборку агента"),
    telebot.types.BotCommand("/nexus_rotation", "удаляет отчеты из нексус, оставляя %ARG% последних"),
    telebot.types.BotCommand("/nexus_upload_reports", "выгрузжает все локальные отчеты в нексус,"
                                                      " чистит локальное хранилище"),
    telebot.types.BotCommand("/nexus_get_reports", "вернет все отчеты в виде файлов"),
    telebot.types.BotCommand("/help", "список доступных команд")
])


def clear_reports() -> int:
    i = 0
    for filename in os.listdir(reports_path):
        i += 1
        os.remove('{0}{1}'.format(reports_path, filename))
    return i


def nexus_upload(chat_id) -> int:
    filenames = os.listdir(reports_path)
    i = 0
    for filename in filenames:
        if filename.endswith('.json'):
            data = open('{0}{1}'.format(reports_path, filename), 'rb').read()
            response = requests.put(
                'http://172.20.73.29:8081/repository/reports/{}'.format(filename),
                data=data,
                auth=(nexus_login, nexus_password))
            if response.ok:
                i += 1
                bot.send_message(chat_id, 'nexus_upload_reports: {0} - success'.format(filename))
                os.remove('{0}{1}'.format(reports_path, filename))
            else:
                bot.send_message(chat_id, 'nexus_upload_reports: {0} - failed'.format(filename))
                return False
    return i


def start_argus_pipeline(chat_id, branch_name) -> bool:
    files = {
        'token': (None, argus_pipeline_token),
        'ref': (None, branch_name),
    }
    response = requests.post('https://gitlab.stc-spb.ru/api/v4/projects/4432/trigger/pipeline', files=files)
    bot.send_message(chat_id, "run_argus_pipeline for branch {0}: {1}".format(branch_name, response.ok))
    return response.ok


def timer_event_reminder():
    bot.send_message(chat_IDs[current_chat_ID],
                     "Напоминание: через 1.5 часа общий отчет будет выгружен в nexus в автоматическом режиме,"
                     "просьба всем - написать и отправить отчеты до этого времени."
                     "Ответсвенным за слияния - подготовить ветку build."
                     "@space_bot_3000, @truelydev, @etozhetek,"
                     "@cyberfantom2000, @Onner7, @jlighten, @IlaySalem")


def timer_event_executor():
    functions.set_timer_event(functions.timer_day, functions.timer_hour)
    chat_id = chat_IDs[current_chat_ID]
    i = nexus_upload(chat_id)
    if i > 0:
        bot.send_message(chat_id, "Загружено отчетов: {}".format(i))
        reply = functions.nexus_rotate(nexus_login, nexus_password, 666)
        bot.send_message(chat_id, reply[1])
        if reply[0]:
            if start_argus_pipeline(chat_id, "build"):
                bot.send_message(chat_id, "Сбор отчетов прошел успешно, всем хороших выходных!")
            else:
                bot.send_message(chat_id, "Не удалось запустить сборку!")
        else:
            bot.send_message(chat_id, "Не удалось выгрузить отчеты!")
    else:
        bot.send_message(chat_id, "nexus_upload - отсутствуют отчеты во временной директории.")


functions.em.on('time_event_reminder', timer_event_reminder)
functions.em.on('time_event_executor', timer_event_executor)

functions.set_timer_event(functions.timer_day, functions.timer_hour)


@bot.message_handler(commands=[command_list[0]])
def get_help(message):
    if message.from_user.id not in admins:
        bot.send_message(message.chat.id, 'выполнение команд запрещено незарегистрированным пользователям')
    else:
        bot.send_message(message.chat.id, 'список доступных команд:\n {}'.format('\n/'.join(map(str, command_list))))


@bot.message_handler(commands=[command_list[1]])
def nexus_get_reports(message):
    if message.from_user.id not in admins:
        bot.send_message(message.chat.id, 'выполнение команд запрещено незарегистрированным пользователям')
    else:
        response = functions.nexus_search(nexus_login, nexus_password)
        if response.ok:
            reply_json = json.loads(response.text)
            for item in reply_json['items']:
                url = item['assets'][0]['downloadUrl']
                file_response = requests.get(url, auth=(nexus_login, nexus_password))
                if file_response.ok:
                    j_content = file_response.content
                    report = Path('./reports/{}'.format(item['name']))
                    report.touch(exist_ok=True)
                    with open(report, 'w+') as document:
                        report.write_bytes(j_content)
                    with open(report, 'r+') as document:
                        document = open(report, 'rb')
                        bot.send_document(message.chat.id, document)
                        document.close()
                    os.remove(report)
                else:
                    bot.send_message(message.chat.id, 'nexus_get_reports - не удалось получить отчет: {}'.format(url))
            if not len(reply_json['items']):
                bot.send_message(message.chat.id, 'nexus_get_reports - репозиторий пуст')
        else:
            bot.send_message(message.chat.id, 'nexus_get_reports - nexus-репозиторий не отвечает')


@bot.message_handler(commands=[command_list[2]])
def nexus_upload_reports(message):
    if message.from_user.id not in admins:
        bot.send_message(message.chat.id, 'выполнение команд запрещено незарегистрированным пользователям')
    else:
        nexus_upload(message.chat.id)


@bot.message_handler(commands=[command_list[3]])
def nexus_rotation(message):
    if message.from_user.id not in admins:
        bot.send_message(message.chat.id, 'выполнение команд запрещено незарегистрированным пользователям')
    else:
        counter = [int(i) for i in message.text.split() if i.isdigit()]
        if len(counter) != 1:
            bot.send_message(message.chat.id, 'невалидный аргумент, введи одно целое число - количество отчетов,'
                                              ' которые следует оставить в репозитории')
            return
        bot.send_message(message.chat.id, functions.nexus_rotate(nexus_login, nexus_password, counter[0])[1])


@bot.message_handler(commands=[command_list[4]])
def run_agent_pipeline(message):
    if message.from_user.id not in admins:
        bot.send_message(message.chat.id, 'выполнение команд запрещено незарегистрированным пользователям')
    else:
        arg = message.text.partition(' ')[2]
        branch = 'build-develop' if not arg else arg
        files = {
            'token': (None, agent_pipeline_token),
            'ref': (None, branch),
        }
        response = requests.post('https://gitlab.stc-spb.ru/api/v4/projects/4440/trigger/pipeline', files=files)
        bot.send_message(message.chat.id, "run_agent_pipeline for branch {0}: {1}".format(branch, response.ok))


@bot.message_handler(commands=[command_list[5]])
def run_argus_pipeline(message):
    if message.from_user.id not in admins:
        bot.send_message(message.chat.id, 'выполнение команд запрещено незарегистрированным пользователям')
    else:
        arg = message.text.partition(' ')[2]
        branch = 'build' if not arg else arg
        start_argus_pipeline(message.chat.id, branch)


@bot.message_handler(commands=[command_list[6]])
def set_timer_event(message):
    if message.from_user.id not in admins:
        bot.send_message(message.chat.id, 'выполнение команд запрещено незарегистрированным пользователям')
    else:
        day = [int(i) for i in message.text.split() if i.isdigit()]
        if len(day) != 2:
            bot.send_message(message.chat.id, 'невалидный аргумент, введи 2 целых числа: 1 - день, 2 - час'
                                              ' (смотри описание команды)')
            return

        if day[0] > 6 or day[0] < 0:
            bot.send_message(message.chat.id, 'set_timer_event - некорректный формат дней, введи число [0..6]')
            return

        if day[1] > 23 or day[1] < 0:
            bot.send_message(message.chat.id, 'set_timer_event - некорректный формат часов, введи число [0..23]')
            return

        functions.set_timer_event(day[0], day[1])
        bot.send_message(message.chat.id, 'set_timer_event - успех\n{0}'.format(functions.current_time()))


@bot.message_handler(commands=[command_list[7]])
def get_timer_event(message):
    if message.from_user.id not in admins:
        bot.send_message(message.chat.id, 'выполнение команд запрещено незарегистрированным пользователям')
    else:
        bot.send_message(message.chat.id, 'get_timer_event - успех\n{0}'.format(functions.current_time()))


@bot.message_handler(commands=[command_list[8]])
def clear_local_reports(message):
    if message.from_user.id not in admins:
        bot.send_message(message.chat.id, 'выполнение команд запрещено незарегистрированным пользователям')
    else:
        bot.send_message(message.chat.id, 'отчетов удалено: {}'.format(clear_reports()))


@bot.message_handler(commands=[command_list[9]])
def get_local_reports(message):
    if message.from_user.id not in admins:
        bot.send_message(message.chat.id, 'выполнение команд запрещено незарегистрированным пользователям')
    else:
        bot.send_message(message.chat.id, 'локальные отчеты: {}'.format(os.listdir(reports_path)))


@bot.message_handler(commands=[command_list[10]])
def get_local_report(message):
    if message.from_user.id not in admins:
        bot.send_message(message.chat.id, 'выполнение команд запрещено незарегистрированным пользователям')
    else:
        file_name = message.text.partition(' ')[2]
        report = Path('{0}{1}'.format(reports_path, file_name))
        if not report.is_file():
            bot.send_message(message.chat.id, '{} - файл не найден'.format(file_name))
        else:
            with open(report, 'r+') as document:
                document = open(report, 'rb')
                bot.send_document(message.chat.id, document)
                document.close()


@bot.message_handler(func=lambda message: True)
def echo_all(message):
    global today
    print('Group {} message received: {}'.format(message.chat.title, message.text))
    if today != date.today().strftime("%d_%m_%Y"):
        today = date.today().strftime("%d_%m_%Y")

    for tag in tags:
        if tag in message.text:
            filename = Path('./reports/{}.json'.format(today))
            filename.touch(exist_ok=True)
            with open(filename, 'r+') as document:
                try:
                    j_obj = json.load(document)
                except:
                    j_obj = {}
            if functions.parse(j_obj, tag, message, filename):
                bot.send_message(message.chat.id, 'отчет @{0} id:{1} сохранён'.format(message.from_user.username,
                                                                                      message.from_user.id))


bot.infinity_polling()
