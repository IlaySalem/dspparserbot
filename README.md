#Docker instruction (example):

```
#pip freeze > requirements.txt

#id: "${CONTAINER_NAME}"
docker compose build
docker compose up 
docker ps -a
#got id and name
docker run --name "${CONTAINER_NAME}" -v /etc/timezone:/etc/timezone -v /etc/localtime:/etc/localtime --restart always -p 80:80 "${IMAGE_NAME}"
```
