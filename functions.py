import json
import requests
import event_emitter as events
from datetime import datetime, timedelta, timezone
from date_time_event import Untiltime

em = events.EventEmitter()


def function_reminder():
    print("{} function_reminder".format(datetime.now()))
    em.emit('time_event_reminder')


def function_executor():
    print("{} function_executor".format(datetime.now()))
    em.emit('time_event_executor')


timer_day = 4
timer_hour = 13
reminder_date_time = datetime.now()
executor_date_time = reminder_date_time + timedelta(0, 0, 0, 0, 30, 1)  # after 1.5 hour

threadReminder = Untiltime(function_reminder, dateOrtime=reminder_date_time)
threadExecutor = Untiltime(function_executor, dateOrtime=executor_date_time)


def count_delta_day(desired_day, desired_hour):
    current_day = datetime.now().weekday()
    if current_day < desired_day or (current_day == desired_day and datetime.now().hour < desired_hour):
        return desired_day - current_day
    else:
        return 7 + desired_day - current_day


def count_delta_hour(desired_hour):
    current_hour = datetime.now().hour
    return desired_hour - current_hour


def set_timer_event(day, hour):
    global reminder_date_time, executor_date_time, timer_day, timer_hour, threadReminder, threadExecutor
    timer_day = day
    timer_hour = hour
    delt_days = count_delta_day(timer_day, hour)
    delt_hours = count_delta_hour(timer_hour)
    print(delt_days, delt_hours)
    reminder_date_time = datetime.now() + timedelta(delt_days, 5, 0, 0, 0,
                                                    delt_hours)
    reminder_date_time = reminder_date_time.replace(minute=0, second=5)
    executor_date_time = reminder_date_time + timedelta(0, 0, 0, 0, 30, 1)

    threadReminder.cancel()
    threadExecutor.cancel()

    threadReminder = Untiltime(function_reminder, dateOrtime=reminder_date_time)
    threadExecutor = Untiltime(function_executor, dateOrtime=executor_date_time)

    threadReminder.start()
    threadExecutor.start()


def current_time() -> str:
    dn = datetime.now()
    return 'текущий день/час: {0}/{1} ({2})\nтаймер день/час: {3}/{4} ({5})'.format(dn.weekday(),
                                                                                    dn.strftime("%H"),
                                                                                    dn.strftime('%d.%m.%Y %H:%M:%S'),
                                                                                    str(reminder_date_time.weekday()),
                                                                                    str(reminder_date_time.strftime(
                                                                                        "%H")),
                                                                                    reminder_date_time.strftime(
                                                                                        '%d.%m.%Y %H:%M:%S'))


def parse(j_obj, tag, message, file_name) -> bool:
    parsed_message = []
    report_list = message.text.split("\n")
    print('tag: {0}, parsed: {1}'.format(tag, report_list))
    if tag not in report_list:
        print('bullshit report: {}'.format(tag, report_list))
        return False
    i = report_list.index(tag) + 1
    while i < len(report_list):
        report_str = report_list[i].partition(' ')[2]
        if ("#" in report_str) or (report_str == "") or report_str.isspace():
            break
        parsed_message.append(report_str)
        i += 1

    dumped_arr = j_obj.get(tag)
    if not dumped_arr:
        dumped_arr = parsed_message
    else:
        dumped_arr.extend(parsed_message)
    j_obj.update({tag: dumped_arr})
    with open(file_name, 'w+') as document:
        json.dump(j_obj, document)

    return True


def nexus_search(nexus_login, nexus_password) -> requests.Response:
    return requests.get('http://172.20.73.29:8081/service/rest/v1/search',
                        auth=(nexus_login, nexus_password))


def nexus_rotate(nexus_login, nexus_password, reports_to_live) -> tuple[bool, str]:
    response = nexus_search(nexus_login, nexus_password)
    if response.ok:
        reply_json = json.loads(response.text)
        dictionary = {}
        file_names_dictionary = {}
        for item in reply_json['items']:
            report_id = item['assets'][0]['id']
            last_modified = item['assets'][0]['lastModified']
            dictionary.update({last_modified: report_id})
            file_names_dictionary.update({last_modified: item['name']})
        sorted_dict = dict(sorted(dictionary.items()))
        reports_counter = len(sorted_dict)
        for key in sorted_dict:
            if (len(sorted_dict) - reports_to_live) <= 0 or reports_counter <= 0 or reports_counter <= reports_to_live:
                return (True, 'nexus_rotation: текущее количество отчетов в репозитории: {},'
                              ' команда выполнена'.format(reports_counter))
            else:
                response = requests.delete('http://172.20.73.29:8081/service/rest/v1/assets/{}'
                                           .format(sorted_dict[key]),
                                           auth=(nexus_login, nexus_password))
                print('nexus_rotation: {0} isRemoved: {1}'.format(file_names_dictionary[key], response.ok))
                if response.ok:
                    reports_counter -= 1
                else:
                    return False, 'nexus_rotation: ошибка запроса, команда не выполнена'
        return False, 'nexus_rotation: отчеты в репозитории отсутствуют'
    else:
        return False, 'nexus_rotation: nexus-сервер недоступен'
